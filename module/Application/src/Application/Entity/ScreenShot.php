<?php
/**
 * Application Module 
 *
 * @link https://github.com/bjyoungblood/BjyAuthorize for the canonical source repository
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
 
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * An example of how to implement a role aware user entity.
 *
 * @ORM\Entity
 * @ORM\Table(name="screenshots")
 *
 * @author Sina Miandashti <miandashti@gmail.com>
 */
class ScreenShot
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string",  length=255)
     */
    protected $thumb;

    /**
     * @var string
     * @ORM\Column(type="string",  length=255)
     */
    protected $newsletter;

    /**
     * @var string
     * @ORM\Column(type="string",  length=255)
     */
    protected $original;
   

   /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $init_time;

   /**
    * Getter for thumb
    *
    * @return mixed
    */
   public function getThumb()
   {
       return $this->thumb;
   }
   
   /**
    * Setter for thumb
    *
    * @param mixed $thumb Value to set
   
    * @return self
    */
   public function setThumb($thumb)
   {
       $this->thumb = $thumb;
       return $this;
   }

   /**
    * Getter for original
    *
    * @return mixed
    */
   public function getOriginal()
   {
       return $this->original;
   }
   
   /**
    * Setter for original
    *
    * @param mixed $original Value to set
   
    * @return self
    */
   public function setOriginal($original)
   {
       $this->original = $original;
       return $this;
   }
   /**
    * Getter for newsletter
    *
    * @return mixed
    */
   public function getNewsletter()
   {
       return $this->newsletter;
   }
   
   /**
    * Setter for newsletter
    *
    * @param mixed $newsletter Value to set
   
    * @return self
    */
   public function setNewsletter($newsletter)
   {
       $this->newsletter = $newsletter;
       return $this;
   }
   
   
    
    /**
     * Getter for init_time
     *
     * @return mixed
     */
    public function getInitTime()
    {
        return $this->init_time;
    }
    
    /**
     * Setter for init_time
     *
     * @param mixed $initTime Value to set
    
     * @return self
     */
    public function setInitTime($initTime)
    {
        $this->init_time = $initTime;
        return $this;
    }
    
    /**
     * Getter for id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Setter for id
     *
     * @param mixed $id Value to set
    
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
}
