<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
    	

    	if ($this->params()->fromQuery('start')) {
    		$stamp = intval($this->params()->fromQuery('start'));
    		$stamp2 = $stamp + (24*3600);
    	} else {
    		$stamp = strtotime('today');
    		$stamp2 = $stamp + (24*3600);
    	}


        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        // $screenshots = $em->getRepository('\Application\Entity\ScreenShot')->findBy(array(), array('id' => 'DESC'));

        $query = $em->createQuery("SELECT u FROM \Application\Entity\ScreenShot u WHERE u.init_time BETWEEN  $stamp AND $stamp2");
		$screenshots = $query->getResult();
        return new ViewModel(array('screenshots' => $screenshots));
    }

    public function archiveAction() {
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $query = $em->createQuery("SELECT MAX(u.init_time),MIN(u.init_time) FROM \Application\Entity\ScreenShot u");
        $res = $query->getResult();

        $biggestTimestamp = intval($res[0][1]);
        $smallestTimestamp = intval($res[0][2]);

    	$numDays = abs($smallestTimestamp - $biggestTimestamp)/60/60/24;
    	$dates = array();
		for ($i = 0; $i < $numDays; $i++) {

	    	$date = date('Y/m/d', strtotime("+{$i} day", $smallestTimestamp));
	    	$timestamp = strtotime("+{$i} day", $smallestTimestamp);
	    	$dates[] = array($date,$timestamp);
		}
        return new ViewModel(array('dates' => $dates));

    }
}
