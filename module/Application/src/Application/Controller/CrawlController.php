<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Dom\Query;
class CrawlController extends AbstractActionController
{
    public function indexAction()
    {
        $htmlfront = $this->curl_get_content("http://www.jaaar.com/frontpage");
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');


        //remove old today
        $stamp = strtotime('today');
    	$stamp2 = $stamp + (24*3600);
        $query = $em->createQuery("SELECT u FROM \Application\Entity\ScreenShot u WHERE u.init_time BETWEEN  $stamp AND $stamp2");
		$screenshots = $query->getResult();
		foreach ($screenshots as $key => $value) {
			# code...
			$em->remove($value);
		}

		$em->flush();

        $dom = new Query($htmlfront);
		$results = $dom->execute('.all');
		$dirname = date('Y-m-d');
		mkdir(getcwd().'/public/data/thumb/'.$dirname,0777);
		mkdir(getcwd().'/public/data/original/'.$dirname,0777);

		foreach ($results as $result) {
			$newslettername = trim($result->getAttribute('id'));
			if ($newslettername == '') continue;
			$htmlcontent = trim($this->getInnerHTML($result));


        	$dominner = new Query($htmlcontent);
			$resultsInner = $dominner->execute('img');
			$resulttitle = $dominner->execute('.thumb-title');
			$titlefromthumb = $resultsInner[0]->getAttribute('title');

        	$titlefromthumb = utf8_decode($titlefromthumb);

			$thumbsrc = $resultsInner[0]->getAttribute('src');

			$fullsrc = str_replace('_thumb', '', $thumbsrc);


			$url = 'http://www.jaaar.com'.$thumbsrc;
			$urlsrc = 'http://www.jaaar.com'.$fullsrc;
			$hashforthumb = md5(time()).'.jpg';
			$hashfortsrc = md5(time()+time()).'.jpg';

			$img = getcwd().'/public/data/thumb/'.$dirname.'/'.$hashforthumb;
			$imgsrc = getcwd().'/public/data/original/'.$dirname.'/'.$hashfortsrc;

			file_put_contents($img, file_get_contents($url));
			file_put_contents($imgsrc, file_get_contents($urlsrc));
			// var_dump($newslettername,$htmlcontent);
			    // $result is a DOMElement


			$screenshot = new \Application\Entity\ScreenShot();
			$screenshot->setNewsletter($titlefromthumb);
			$screenshot->setThumb($hashforthumb);
			$screenshot->setOriginal($hashfortsrc);
			$screenshot->setInitTime(time());

        	$em->persist($screenshot);
		}

		$em->flush();
        exit;
    }

    public function curl_get_content($url) {
    	$ch = curl_init($url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    	$content = curl_exec($ch);
    	curl_close($ch);
    	return $content;
    }

    public function getInnerHTML($node)
	{
	    $innerHTML= ''; 
	    $children = $node->childNodes; 
	    foreach ($children as $child) { 
	        $innerHTML .= $child->ownerDocument->saveXML( $child ); 
	    } 

	    return $innerHTML;
	}
}
